import React from "react";
import ReactDOM from "react-dom";
import { HashRouter, Routes, Route } from "react-router-dom";
import reportWebVitals from "./reportWebVitals";
import CheckoutPage from "./Components/CheckoutPage";
import MerchantPage from "./Components/MerchantPage";

const App = () => {
  return (
    <HashRouter>
      <Routes>
        <Route
          index
          element={
              <CheckoutPage/>
          }
          
        />
        <Route
          path="/MerchantPage"
          element={
              <MerchantPage />
          }
        />
      </Routes>
      </HashRouter>
  );
}
export default App;
